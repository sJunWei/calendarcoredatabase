﻿using System.ComponentModel.DataAnnotations.Schema;

namespace calendarDatabaseAPI.Models
{
    public class teamCalendar
    {
        public int id { get; set; }
        [ForeignKey("team_id")]
        public int team_id { get; set; }
        [ForeignKey("calendar_id")]
        public int calendar_id { get; set; }
    }
}
