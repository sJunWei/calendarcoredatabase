﻿using System.ComponentModel.DataAnnotations.Schema;

namespace calendarDatabaseAPI.Models
{
    public class appointmentNotification
    {
        public int id { get; set; }
        [ForeignKey("appointment_id")]
        public int appointment_id { get; set; }
        [ForeignKey("notification_id")]
        public int notification_id { get; set; }
    }
}
