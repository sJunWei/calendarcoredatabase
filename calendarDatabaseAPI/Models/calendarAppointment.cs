﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace calendarDatabaseAPI.Models
{
    public class calendarAppointment
    {
        public int id { get; set; }
        public DateTime createdAt { get; set; }
        [ForeignKey("calendar_id")]
        public int calendar_id { get; set; }
        [ForeignKey("appointment_id")]
        public int appointment_id { get; set; }

    }
}
