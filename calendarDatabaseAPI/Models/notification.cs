﻿using System;

namespace calendarDatabaseAPI.Models
{
    public class notification
    {
        public int Id { get; set; }
        public DateTime notifyDateTime { get; set; }
        public string method { get; set; }
        public string value { get; set; }

    }
}
