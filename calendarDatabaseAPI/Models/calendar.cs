﻿namespace calendarDatabaseAPI.Models
{
    public class calendar
    {
        public int id { get; set; }
        public string calendarTitle { get; set; }
        public string calendarDescription { get; set; }
    }
}
