﻿using System;

namespace calendarDatabaseAPI.Models
{
    public class appointment
    {
        public int id { get; set; }
        public string appointmentTitle { get; set; }
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
        public string status { get; set; }
    }
}
