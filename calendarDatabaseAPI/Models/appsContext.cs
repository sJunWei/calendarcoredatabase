﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace calendarDatabaseAPI.Models
{
    public partial class appsContext : DbContext
    {
        public appsContext()
        {

        }

        public appsContext(DbContextOptions<appsContext> options) : base(options)
        {

        }

        public virtual DbSet<users> users { get; set; } //  add from model
        public virtual DbSet<calendar> calendar { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // add from model
            modelBuilder.Entity<users>(b =>
            {
                b.ToTable("users");
            });
            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

    }
}
