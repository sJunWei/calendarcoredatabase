﻿using System.ComponentModel.DataAnnotations.Schema;

namespace calendarDatabaseAPI.Models
{
    public class eventNotification
    {
        public int id { get; set; }
        [ForeignKey("event_id")]
        public int event_id { get; set; }
        [ForeignKey("notification_id")]
        public int notification_id { get; set; }
    }
}
