﻿using System.ComponentModel.DataAnnotations.Schema;

namespace calendarDatabaseAPI.Models
{
    public class userNotification
    {
        public int id { get; set; }
        [ForeignKey("users_id")]
        public int users_id { get; set; }
        [ForeignKey("notification_id")]
        public int notification_id { get; set; }
    }
}
