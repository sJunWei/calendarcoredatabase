﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace calendarDatabaseAPI.Models
{
    public class calendarEvent
    {
        public int id { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string role { get; set; }
        [ForeignKey("calendar_id")]
        public int calendar_id { get; set; }
        [ForeignKey("event_id")]
        public int event_id { get; set; }
    }
}
