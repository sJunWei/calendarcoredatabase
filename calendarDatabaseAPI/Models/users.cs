﻿using System;
using System.ComponentModel.DataAnnotations;

namespace calendarDatabaseAPI.Models
{
    public class users
    {
        //[Key]
        //public int userId { get; set; }
        public int id { get; set; }

        public string userFirstName { get; set; }

        public string userLastName { get; set; }

        public string userName { get; set; }

        [EmailAddress]
        public string userEmail { get; set; }

        public string userMobile { get; set; } = String.Empty;//this is when user no enter and it will change to null.

        public string passwordHash { get; set; }

        public DateTime registerAt { get; set; }
    }
}
