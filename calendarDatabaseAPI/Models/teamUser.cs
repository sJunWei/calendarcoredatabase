﻿using System.ComponentModel.DataAnnotations.Schema;

namespace calendarDatabaseAPI.Models
{
    public class teamUser
    {
        public int id { get; set; }
        [ForeignKey("team_id")]
        public int team_id { get; set; }
        [ForeignKey("users_id")]
        public int users_id { get; set; }
        [ForeignKey("role_id")]
        public int role_id { get; set; }
    }
}
