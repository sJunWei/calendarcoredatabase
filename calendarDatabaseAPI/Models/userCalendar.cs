﻿using System.ComponentModel.DataAnnotations.Schema;

namespace calendarDatabaseAPI.Models
{
    public class userCalendar
    {
        public int id { get; set; }
        [ForeignKey("users_id")]
        public int users_id { get; set; }
        [ForeignKey("calendar_id")]
        public int calendar_id { get; set; }
    }
}
