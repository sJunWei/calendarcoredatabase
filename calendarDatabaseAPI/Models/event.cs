﻿using System;

namespace calendarDatabaseAPI.Models
{
    public class @event
    {
        public int id { get; set; }
        public string eventName { get; set; }
        public string eventDescription { get; set; }
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
        public bool allDay { get; set; }
    }
}
