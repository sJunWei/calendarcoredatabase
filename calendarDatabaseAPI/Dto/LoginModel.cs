﻿using System.ComponentModel.DataAnnotations;

namespace calendarDatabaseAPI.Dto
{
    public class LoginModel
    {
        [Required(ErrorMessage = "User Name is required")]
        public string? userName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string? passwordHash { get; set; }
    }
}
