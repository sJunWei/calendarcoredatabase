﻿using calendarDatabaseAPI.Dto;
using calendarDatabaseAPI.Models;
using calendarDatabaseAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace calendarDatabaseAPI.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class authController : ControllerBase
    {
        private readonly appsContext _context;
        IConfiguration configuration;
        UserService userService;

        public authController(appsContext context, IConfiguration configuration, UserService userService)
        {
            _context = context;
            this.configuration = configuration;
            this.userService = userService;
        }

        [HttpPost]
        [Route("login")]
        //POST: /api/auth/login
        public IActionResult Login([FromBody] LoginModel model)
        {
            var user = userService.GetUser(model.userName ?? "");

            if (user != null)
            {
                if (!userService.CheckPassword(user, model.passwordHash ?? ""))
                    return Unauthorized();

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: configuration["JWT:ValidIssuer"],
                    audience: configuration["JWT:ValidAudience"],
                    //expires: DateTime.Now.AddHours(3),
                    claims: new List<Claim>(),
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("test")]
        public IActionResult test([FromBody] LoginModel model)
        {
            return Ok(model);
        }

        //[HttpPost, Route("login")]
        //public IActionResult Login([FromBody] users user)
        //{
        //    if (user is null)
        //    {
        //        return BadRequest("Invalid client request");
        //    }
        //    //compare username & password
        //    var data = _context.users.Where(d=>d.userName == user.userName && d.passwordHash == user.passwordHash).FirstOrDefault(); 

        //    if (user.userName == data.userName && user.passwordHash == data.passwordHash)
        //    {
        //        var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
        //        var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
        //        var tokeOptions = new JwtSecurityToken(
        //            issuer: "https://localhost:5001",
        //            audience: "https://localhost:5001",
        //            claims: new List<Claim>(),
        //            //expires: DateTime.Now.AddMinutes(5),
        //            signingCredentials: signinCredentials
        //        );

        //        var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

        //        return Ok(new { Token = tokenString });
        //    }

        //    return Unauthorized();
        //}

    }
}
