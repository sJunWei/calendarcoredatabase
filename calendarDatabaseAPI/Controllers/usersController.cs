﻿using calendarDatabaseAPI.Dto;
using calendarDatabaseAPI.Models;
using calendarDatabaseAPI.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace calendarDatabaseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class usersController : ControllerBase
    {
        private readonly appsContext _context;
        IConfiguration configuration;
        UserService userService;

        public usersController(appsContext context, IConfiguration configuration, UserService userService)
        {
            _context = context;
            this.configuration = configuration;
            this.userService = userService;
        }

        [HttpGet]
        [Route("getUsers")]
        public object getUsers()
        {
            var data = _context.users.ToList();
            return data;
        }

        //[HttpGet]
        //[Route("userProfile")]
        //public async Task<IActionResult> getUserProfile()
        //{
        //    string userId = userService.Claims.First(c => c.Type == "UserID").value;
        //    var user = _context.FindByIdAync
        //}
    }
}
