﻿using calendarDatabaseAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace calendarDatabaseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class calendarController : ControllerBase
    {
        private readonly appsContext _context;

        public calendarController(appsContext context)
        {
            _context = context;
        }
        [HttpGet]
        [Route("getCalendar")]
        public object getCalendar()
        {
            var data = _context.calendar.ToList();
            return data;
        }
    }
}
