﻿using calendarDatabaseAPI.Models;
using System;
using System.Linq;
using System.Security.Claims;

namespace calendarDatabaseAPI.Service
{
    public class UserService
    {
        appsContext context;

        public UserService(appsContext context)
        {
            this.context = context;
        }

        public users GetUser(string userName)
        {
            var user = context.users.Where(a => a.userName == userName).FirstOrDefault();
            return user;
        }

        public bool CheckPassword(users user, string password)
        {
            return user.passwordHash == password;
        }

        public users GetUser(ClaimsPrincipal claimsPrincipal)
        {
            var identity = (ClaimsIdentity)(claimsPrincipal.Identity ?? new ClaimsIdentity());
            var claim = identity.FindFirst(ClaimTypes.Name);
            if (claim != null)
            {
                var user = GetUser(claim.Value);
                if (user != null)
                    return user;
            }
            throw new Exception("Invalid user");
        }
    }
}


